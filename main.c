#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include "serialdevice.h"


static const uint8_t command[8] = {0x10, 0x03, 0x00, 0x63, 0x00, 0x03, 0xf6, 0x94};

void communicate(FILE *f)
{
    printf("%i bytes written\n", fwrite(&command, 1, 8, f));
    for (size_t i = 0; i < 8; i++) {printf("%x ", command[i]);}
    printf("\nsent.\n");
    rewind(f);

    uint8_t retrieved[8] = {0};
    printf("Now read the answer\n");
    size_t read_bytes = fread(&retrieved, 1, 8, f);
    printf("\nRetrieved (%zi):\n\n", read_bytes);
    if (read_bytes > 0)
    {
        for (size_t i = 0; i < read_bytes; i++) {printf("%x ", retrieved[i]);}
    }
    printf("\nend.\n");
}

int main()
{
    FILE *f = devconnect("/dev/ttyO1");

    communicate(f);
    communicate(f);

    fclose(f);

    return 0;
}
