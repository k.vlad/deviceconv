#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <termios.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "serialdevice.h"


#define TERMIOS_PRINT   1

#if TERMIOS_PRINT == 1

// verbose field-by-field print

#define printtermios(t, title) {    \
    char s[32];                     \
    snprintf(s, 32, "%s", title);   \
    printf("\n%s\n"                 \
           "chars: %s\n"            \
           "cflag: %u\n"            \
           "iflag: %u\n"            \
           "ispeed: %u\n"           \
           "lflag: %u\n"            \
           "line: %c\n"             \
           "oflag: %u\n"            \
           "ospeed %u\n",           \
            s,                      \
            t.c_cc,                 \
            t.c_cflag,              \
            t.c_iflag,              \
            t.c_ispeed,             \
            t.c_lflag,              \
            t.c_line,               \
            t.c_oflag,              \
            t.c_ospeed);            \
}

#elif TERMIOS_PRINT == 2

// print as bytes

#define printtermios(t, title) {                \
    char s[32];                                 \
    size_t size = sizeof(t);                    \
    snprintf(s, 32, "%s\nsz %d\n", title, size);\
    printf("%s", s);                            \
    uint8_t *ptr = (uint8_t*)(&t);              \
    for (size_t i = 0; i < size; i++) {         \
        printf("%x ", *(ptr + i));              \
    }                                           \
    printf("\n");                               \
}

#else

#define printtermios(t, title)  ;

#endif

void devinit(int fd)
{
    // create appropriate list of parameters and store there terminal attributes
    struct termios t;
    switch (tcgetattr(fd, &t))
    {
    case -1:
        printf("%s, line %i", strerror(errno), __LINE__);
        break;
    default:
        printtermios(t, "Before:");
        break;
    }

    // change baud rate correctly
    cfsetspeed(&t, B9600);

    //  hardware control - 8 data bits
    t.c_cflag = (t.c_cflag & ~CSIZE) | CS8;
    //  hardware control - 1 stop bit
    t.c_cflag &= ~CSTOPB;

    // various terminal functions - not used
    t.c_lflag = 0;
    // output flags - not used
    t.c_oflag = 0;
    // special characters
    t.c_cc[VTIME] = 1;
    t.c_cc[VMIN] = 60;

    t.c_cflag |= CLOCAL | CREAD;

    // input flags - no software handshake
    t.c_iflag &= ~(IXON|IXOFF|IXANY);

    //  hardware control - no hardware handshake
    t.c_cflag &= ~CRTSCTS;

    // And apply settings immediatly
    if (tcsetattr(fd, TCSANOW, &t) != 0)
    {
        printf("%s, line %i", strerror(errno), __LINE__);
    }

    printtermios(t, "After:");
}

FILE* devconnect(char const* filename)
{
    FILE *dev = NULL;

    // c-string overflow protection
    char _filename[256];
    snprintf(_filename, 256, "%s", filename);

    dev = fopen(_filename, "a+");
    if (dev == NULL)
    {
        printf("Device access error %s\n", strerror(errno));
    }
    else
    {
        setbuf(dev, NULL); // non-buffered I/O
        int fd = fileno(dev);
        if (fd < 0)
        {
            printf("Failed to get file desc. %s\n", strerror(errno));
            exit(-1);
        }
        printf("Connected\n");
        devinit(fd);
        rewind(dev);
        tcflush(fd, TCIOFLUSH);
    }
    return dev;
}
